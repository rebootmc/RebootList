package dev.creepah.list;

import tech.rayline.core.plugin.RedemptivePlugin;

public final class RebootList extends RedemptivePlugin {

    private static RebootList instance;
    public static RebootList get() {
        return instance;
    }

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;

        registerCommand(new ListCommand());
    }
}
