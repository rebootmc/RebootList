package dev.creepah.list;

import com.google.common.base.Joiner;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.kitteh.vanish.VanishPlugin;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

import java.util.ArrayList;
import java.util.List;

@CommandPermission("reboot.list")
@CommandMeta(description = "List online players", aliases = {"who", "online"})
public final class ListCommand extends RDCommand {

    public ListCommand() {
        super("list");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        List<String> list = RebootList.get().getConfig().getStringList("message-list");
        for (String msg : list) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg
                    .replace("{online}", String.valueOf(Bukkit.getOnlinePlayers().size()))
                    .replace("{max}", String.valueOf(Bukkit.getServer().getMaxPlayers()))
                    .replace("{staff}", getOnlineStaff()).replace("{donators}", getOnlineDonators())));
        }
    }

    public String getOnlineStaff() {
        List<String> staff = new ArrayList<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission("customlist.staff")) {
                if (VanishPlugin.getPlugin(VanishPlugin.class).getManager().isVanished(player)) continue;
                staff.add(player.getName());
            }
        }

        return staff.isEmpty() ? "None" : Joiner.on(", ").join(staff);
    }

    public String getOnlineDonators() {
        List<String> donors = new ArrayList<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission("customlist.donor")) {
                if (player.isOp() || player.hasPermission("customlist.staff")) continue;
                donors.add(player.getName());
            }
        }

        return donors.isEmpty() ? "None" : Joiner.on(", ").join(donors);
    }
}
